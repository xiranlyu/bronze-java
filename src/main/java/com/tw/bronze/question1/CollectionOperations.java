package com.tw.bronze.question1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Arrays;

public class CollectionOperations {
    /**
     * This method will remove all the duplicated `String` values from a
     * collection.
     *
     * @param values The collection which may contain duplicated values
     * @return A new array removing the duplicated values.
     */
    public static String[] distinct(Iterable<String> values) {
        // TODO:
        //  Please implement the method. If you have no idea what Iterable<T>
        //  is, please refer to the unit test, or you can have a search on
        //  the internet, or you can find it in our material.
        // <-start-
        if (values == null) {
            throw new IllegalArgumentException();
        } else if (!values.iterator().hasNext()) {
            return new String[0];
        } else {
            ArrayList<String> finalArray = new ArrayList<String>();
            for (String value: values) {
                if (!finalArray.contains(value)) {
                    finalArray.add(value);
                }
            }
            String[] helpArray = null;
            helpArray = finalArray.toArray(new String[0]);
            Arrays.sort(helpArray);
            return helpArray;
        }
        // --end->
    }
}